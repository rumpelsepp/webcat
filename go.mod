module git.sr.ht/~rumpelsepp/webcat

go 1.15

require (
	codeberg.org/rumpelsepp/helpers v0.0.0-20210716071558-e9191903beef
	git.sr.ht/~sircmpwn/getopt v0.0.0-20201218204720-9961a9c6298f
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/sys v0.0.0-20210105210732-16f7687f5001 // indirect
)
